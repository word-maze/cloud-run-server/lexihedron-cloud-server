using System;
using DataCore.Interfaces;
using FirestoreDB;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace test_server
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddResponseCompression(options =>
            {
                options.Providers.Add<GzipCompressionProvider>();
            });
            services.AddSingleton<IServer, ServerLogic.ServerLogic>(provider => new ServerLogic.ServerLogic(new FirestoreLoader()));
            services.AddControllers().AddNewtonsoftJson(options => options.SerializerSettings.ContractResolver = new ShouldSerializeContractResolver());
            services.AddSwaggerDocument(config =>
            {
                config.PostProcess = document =>
                {
                    document.Info.Version = "v3";
                    document.Info.Title = "Lexihedron API";
                    document.Info.Description = "The API to interact with Lexihedron's database.";
                    document.Info.TermsOfService = "Nope.";
                    document.Info.Contact = new NSwag.OpenApiContact
                    {
                        Name = "Joe",
                        Email = string.Empty,
                        Url = "https://lexihedron.web.app/"
                    };
                };
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3();

            app.UseRouting();
            app.UseResponseCompression();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapGet("/", async context =>
                {
                    String target = Environment.GetEnvironmentVariable("TARGET") ?? "Test";
                    await context.Response.WriteAsync($"Hello {target}!");
                });
            });
        }
    }
}
