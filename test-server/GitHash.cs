﻿using System;
using System.IO;
using System.Reflection;

namespace test_server
{
    public static class GitHash
    {
        public static String GetResourceTextFile(String filename)
        {
            String result;

            using (Stream stream = Assembly.GetCallingAssembly()
                .GetManifestResourceStream("test_server." + filename))
            {
                using StreamReader sr = new StreamReader(stream);
                result = sr.ReadToEnd();
            }

            return result.Trim();
        }

        internal static String _SHA;

        public static String CommitSHA => _SHA ??= GetResourceTextFile("GitHash.txt");
    }
}
