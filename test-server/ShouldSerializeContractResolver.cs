﻿using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace test_server
{
    public class ShouldSerializeContractResolver : DefaultContractResolver
    {
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            JsonProperty property = base.CreateProperty(member, memberSerialization);

            var internalProperty = property.DeclaringType.GetProperty(property.PropertyName);

            if (internalProperty != null && !internalProperty.CanWrite)
            {
                property.ShouldSerialize = instance => false;
            }

            return property;
        }

        
    }
}
