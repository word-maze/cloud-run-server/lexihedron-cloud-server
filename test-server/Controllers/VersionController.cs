﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading.Tasks;
using DataCore.DataSystem;
using DataCore.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace test_server.Controllers
{
    /// <summary>
    /// The API for tracking client and server versions, to ensure compatibility.
    /// </summary>
    [Route("api/version")]
    [ApiController]
    public class VersionController : ControllerBase
    {
        private readonly IServer _server;

        public VersionController(IServer server)
        {
            _server = server;
        }

        /// <summary>
        /// For a given version of the client, get the current version of the server.
        /// </summary>
        /// <param name="clientVersion">The calling version of the client.</param>
        /// <returns></returns>
        [HttpGet("{clientVersion}")]
        public async Task<ActionResult<VersionData>> GetAssociatedVersion(Version clientVersion)
        {
            return await _server.VersionAPI.GetAssociatedVersion(clientVersion);
        }

        /// <summary>
        /// Register a new version of the server.
        /// </summary>
        /// <returns></returns>
        [HttpPost("new")]
        public async Task<ActionResult> Declare()
        {
            var serverVersion = Version.Parse(FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion);
            String url = ControllerContext.HttpContext.Request.Host.ToUriComponent();
            await _server.VersionAPI.Declare(serverVersion, url);
            return NoContent();
        }

        /// <summary>
        /// Connects a version of the client with the current version of the server.
        /// </summary>
        /// <param name="clientVersion">The client version.</param>
        /// <returns></returns>
        [HttpPost("{clientVersion}/associate"), ApiExplorerSettings(IgnoreApi = true)]
        public async Task<ActionResult> AssociateVersions(Version clientVersion)
        {
            var serverVersion = Assembly.GetEntryAssembly().GetName().Version;
            await _server.VersionAPI.AssociateVersions(clientVersion, serverVersion);
            return NoContent();
        }
        /// <summary>
        /// Get the identifying information for this build of the server.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<String>> GetHash()
        {
            var serverVersion = Version.Parse(FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion);
            return $"{GitHash.CommitSHA}:{serverVersion}";
        }
    }
}