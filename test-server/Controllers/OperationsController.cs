﻿using System;
using System.Threading.Tasks;
using DataCore.Interfaces;
using DataCore.Responses;
using Microsoft.AspNetCore.Mvc;

namespace test_server.Controllers
{
    /// <summary>
    /// An API for different actions that can be applied to the database.
    /// </summary>
    [Route("api/operations")]
    [ApiController]
    public class OperationsController : Controller
    {
        private readonly IServer _server;

        public OperationsController(IServer server)
        {
            _server = server;
        }

        /// <summary>
        /// Generates the daily puzzles, for the next day.
        /// Should only be called via a scheduling system.
        /// Not accessible to users.
        /// </summary>
        /// <returns>Nothing.</returns>
        [HttpPost("daily"), ApiExplorerSettings(IgnoreApi = true)]
        public async Task<ActionResult> GenerateDailyPuzzles()
        {
            await _server.OperationsAPI.GenerateDailyPuzzles();
            return NoContent();
        }

        /// <summary>
        /// Checks a series of words against a specific puzzle.
        /// </summary>
        /// <param name="date">The date reference of the puzzle.</param>
        /// <param name="shape">The shape index of the puzzle.</param>
        /// <param name="uid">The user requesting the check.</param>
        /// <param name="words">The words to check, they contain the positions on the puzzle, as well.</param>
        /// <returns></returns>
        [HttpPost("{date}/{shape:int}/{uid}/batchcheck")]
        public async Task<ActionResult<FoundPuzzleWords>> BatchCheck(String date, Int32 shape, String uid, [FromBody] WordList words)
        {
            return await _server.OperationsAPI.BatchCheck(date, shape, uid, words);
        }

        /// <summary>
        /// Check a single word against a specific puzzle.
        /// </summary>
        /// <param name="date">The date reference for the puzzle.</param>
        /// <param name="shape">The shape index of the puzzle.</param>
        /// <param name="uid">The user requesting the check.</param>
        /// <param name="word">The word, including positions.</param>
        /// <returns>Pass/Fail + points awarded, in case we miscalculated.</returns>
        [HttpPost("{date}/{shape:int}/{uid}/check")]
        public async Task<ActionResult<FoundWord>> CheckWord(String date, Int32 shape, String uid, [FromBody] WordData word)
        {
            return await _server.OperationsAPI.CheckWord(date, shape, uid, word);
        }

        /// <summary>
        /// Provides a list of current hints, and creates new one as well.
        /// </summary>
        /// <param name="date">The date reference for the puzzle.</param>
        /// <param name="shape">The shape index of the puzzle.</param>
        /// <param name="uid">The user.</param>
        /// <param name="buy">Whether to generate a new hint.</param>
        /// <returns>A list of uncompleted hints.</returns>
        [HttpPost("{date}/{shape:int}/{uid}/hint")]
        public async Task<ActionResult<WordList>> RequestHint(String date, Int32 shape, String uid, Boolean buy)
        {
            return await _server.OperationsAPI.RequestHint(date, shape, uid, buy);
        }
    }
}
