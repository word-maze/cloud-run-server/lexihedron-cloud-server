﻿using System;
using System.Threading.Tasks;
using DataCore.DataSystem;
using DataCore.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace test_server.Controllers
{
    /// <summary>
    /// An API for data requests specific to the user.
    /// </summary>
    [Route("api/user")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly IServer _server;

        public UserController(IServer server)
        {
            _server = server;
        }

        /// <summary>
        /// Creates a new user, and returns their GUID.
        /// </summary>
        /// <param name="uid">A suggestion. Currently ignored.</param>
        /// <returns>The new GUID for the user.</returns>
        [HttpGet("new")]
        public async Task<ActionResult<String>> CreateNewUser()
        {
            return await _server.UserAPI.CreateNewUser();
        }

        /// <summary>
        /// Combines two users, in the event a user players under a different name.
        /// </summary>
        /// <param name="oldUser">The user to merge to.</param>
        /// <param name="newUser">The user to merge from.</param>
        /// <returns>Nothing.</returns>
        [HttpPost("{oldUser}/combine/{newUser}")]
        public async Task<ActionResult> Combine(String oldUser, String newUser)
        {
            await _server.UserAPI.Combine(oldUser, newUser);
            return NoContent();
        }

        /// <summary>
        /// Announce a startup of the game.
        /// </summary>
        /// <param name="uid">The user playing.</param>
        /// <returns>Nothing.</returns>
        [HttpPost("{uid}/announce")]
        public async Task<ActionResult> Announce(String uid)
        {
            await _server.UserAPI.Announce(uid);
            return NoContent();
        }

        /// <summary>
        /// Get the user's data.
        /// </summary>
        /// <param name="uid">The user.</param>
        /// <returns>Their data.</returns>
        [HttpGet("{uid}")]
        public async Task<ActionResult<UserData>> GetUserData(String uid)
        {
            return await _server.UserAPI.GetUserData(uid);
        }
    }
}
