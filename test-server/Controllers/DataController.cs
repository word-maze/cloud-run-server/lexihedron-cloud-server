﻿using System;
using System.Threading.Tasks;
using DataCore.DataSystem;
using DataCore.Interfaces;
using DataCore.Responses;
using Microsoft.AspNetCore.Mvc;

namespace test_server.Controllers
{
    /// <summary>
    /// The API for non-user data requests.
    /// </summary>
    [Route("api/data")]
    [ApiController]
    public class DataController : Controller
    {
        private readonly IServer _server;

        public DataController(IServer server)
        {
            _server = server;
        }

        /// <summary>
        /// Get the data for a specific puzzle.
        /// </summary>
        /// <param name="date">The date reference.</param>
        /// <param name="shape">The shape index.</param>
        /// <returns>The PuzzleMap object.</returns>
        [HttpGet("{date}/{shape:int}/map/{uid}")]
        public async Task<ActionResult<PuzzleMap>> GetPuzzleMap(String date, Int32 shape, String uid)
        {
            return await _server.DataAPI.GetPuzzleMap(date, shape, uid);
        }

        /// <summary>
        /// Get the json for the specific shape.
        /// </summary>
        /// <param name="shape">0 : Tetrahedron
        /// 1 : Cube
        /// 2 : Octahedron
        /// 3 : Icosahedron
        /// 4 : Dodecahedron</param>
        /// <returns>The json string.</returns>
        [HttpGet("shape/{shape:int}")]
        public async Task<ActionResult<String>> GetShapeData(Int32 shape)
        {
            return await _server.DataAPI.GetShapeData(shape);
        }

        /// <summary>
        /// Gets the words found for this puzzle.
        /// </summary>
        /// <param name="date">The date reference.</param>
        /// <param name="shape">The shape index.</param>
        /// <param name="uid">The user we are checking.</param>
        /// <returns>A list of words.</returns>
        [HttpGet("{date}/{shape:int}/{uid}/words")]
        public async Task<ActionResult<FoundPuzzleWords>> GetFoundWords(String date, Int32 shape, String uid)
        {
            return await _server.DataAPI.GetFoundWords(date, shape, uid);
        }
    }
}
