using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using DataCore.Interfaces;
using DataCore.Responses;
using NUnit.Framework;

namespace LocalTest
{
    public abstract class ServerTestPattern
    {
        protected IServer Server;
        private readonly Dictionary<MethodInfo, Boolean> _methodTest = new Dictionary<MethodInfo, Boolean>();

        [SetUp]
        public abstract void Setup();

        public async Task<T> TestMethod<T>(Expression<Func<Task<T>>> getter)
        {
            MethodCallExpression method;
            try
            {
                method = getter.Body as MethodCallExpression;
                Assert.IsTrue(method != null, nameof(method) + " = null");
                if (!_methodTest.ContainsKey(method.Method))
                {
                    _methodTest.Add(method.Method, true);
                }
            }
            catch (InvalidCastException e)
            {
                Assert.Fail("This expression is not a direct method call. Please use this to call an individual method from the Server member.");
                throw e;
            }


            var compiled = getter.Compile();

            Stopwatch timer = Stopwatch.StartNew();

            var result = await compiled();

            timer.Stop();
            String debugData =
                $"{method.Method.DeclaringType.Name} {method.Method.Name}: {timer.ElapsedTicks / 10.0:##,###} us";
            TestContext.Progress.WriteLine(debugData);
            Console.Out.WriteLine(debugData);

            return result;
        }

        public async Task TestMethod(Expression<Func<Task>> getter)
        {
            MethodCallExpression method;
            try
            {
                method = getter.Body as MethodCallExpression;
                Assert.IsTrue(method != null, nameof(method) + " = null");
                _methodTest.Add(method.Method, true);
            }
            catch (InvalidCastException e)
            {
                Assert.Fail("This expression is not a direct method call. Please use this to call an individual method from the Server member.");
                throw e;
            }

            var compiled = getter.Compile();

            Stopwatch timer = Stopwatch.StartNew();

            await compiled();

            timer.Stop();
            String debugData =
                $"{method.Method.DeclaringType.Name} {method.Method.Name}: {timer.ElapsedTicks / 10.0:##,###} us";
            TestContext.Progress.WriteLine(debugData);
            Console.Out.WriteLine(debugData);
        }

        [Test]
        public async Task Test1()
        {
            Version testVersion = Version.Parse("1.0.0.0");
            // Version API
            await TestMethod(() => Server.VersionAPI.Declare(testVersion, ""));
            await TestMethod(() => Server.VersionAPI.AssociateVersions(testVersion, testVersion));
            await TestMethod(() => Server.VersionAPI.GetAssociatedVersion(testVersion));


            // User API
            String uid = await TestMethod(() => Server.UserAPI.CreateNewUser());
            var user = await TestMethod(() => Server.UserAPI.GetUserData(uid));
            await TestMethod(() => Server.UserAPI.Announce(uid));
            await TestMethod(() => Server.UserAPI.Combine(uid, "amerigo"));

            String dateRef = DateTime.UtcNow.ToString("yyyy-MM-dd");

            // Setup puzzles.
            await TestMethod(() => Server.OperationsAPI.GenerateDailyPuzzles());

            // Data API
            await TestMethod(() => Server.DataAPI.GetShapeData(0));
            await TestMethod(() => Server.DataAPI.GetPuzzleMap(dateRef, 0, uid));
            await TestMethod(() => Server.DataAPI.GetFoundWords(dateRef, 0, uid));

            // Operations API
            WordList wordList = await TestMethod(() => Server.OperationsAPI.RequestHint(dateRef, 0, uid, true));
            WordData testWord = null;

            if (wordList.Words.Length > 0)
            {
                testWord = wordList.Words[0];
            }
            else
            {
                testWord = new WordData("lol", 0, new Int64[] {0, 1, 2});
            }

            await TestMethod(() => Server.OperationsAPI.CheckWord(dateRef, 0, uid, testWord));
            await TestMethod(() => Server.OperationsAPI.BatchCheck(dateRef, 0, uid, wordList));

            await CheckMethods();
        }

        public async Task CheckMethods()
        {
            foreach (var property in typeof(IServer).GetProperties())
            {
                if (property.GetValue(Server) == null)
                {
                    continue;
                }
                foreach (var method in property.PropertyType.GetMethods())
                {
                    if (!_methodTest.GetValueOrDefault(method, false))
                    {
                        Assert.Fail($"{method.Name} was not called during this test. Each method on the Server member needs to be called to ensure full test coverage.");
                        throw new Exception();
                    }
                }
            }
        }
    }
}