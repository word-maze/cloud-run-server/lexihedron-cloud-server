﻿using System;
using Communication;
using Newtonsoft.Json;
using NUnit.Framework;

namespace LocalTest
{
    class RemoteTest : ServerTestPattern
    {
        class TextSerialiser : ISerialiser
        {
            public T Deserialise<T>(String data) where T : class, new()
            {
                return JsonConvert.DeserializeObject<T>(data);
            }

            public String Serialise(Object data)
            {
                return JsonConvert.SerializeObject(data);
            }
        }

        [SetUp]
        public override void Setup()
        {
            String baseURL = Environment.GetEnvironmentVariable("BASEURL") ?? "http://127.0.0.1:5000";
            Server = new Communicator(baseURL, new TextSerialiser());
        }
    }
}
