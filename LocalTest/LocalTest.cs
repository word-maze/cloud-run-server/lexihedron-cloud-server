﻿using DataCore.Classes;
using NUnit.Framework;

namespace LocalTest
{
    class LocalTest : ServerTestPattern
    {
        [SetUp]
        public override void Setup()
        {
            Server = new ServerLogic.ServerLogic(new LocalLoader());
        }
    }
}
